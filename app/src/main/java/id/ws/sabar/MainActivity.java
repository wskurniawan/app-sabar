package id.ws.sabar;

import android.graphics.Color;
import android.os.CountDownTimer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.Calendar;

public class MainActivity extends AppCompatActivity {

    LinearLayout mainLayout;
    ProgressBar progressBar;
    TextView txtLoading;

    CountDownTimer timer, timer2;

    private String strWarna = "000000";
    private int decimalWarna = 0;
    int counter = 10;
    int kodeWarna = 0;

    private boolean mundur = true;
    static String orange[] = {"#ff9900", "#e58900", "#cc7a00", "#b26b00", "#995b00", "#7f4c00", "#663d00", "#4c2d00",
        "#331e00", "#190f00", "#000000"};

    static String merah[] = {"#e50914", "#ce0812", "#b70710", "#a0060e", "#89050c", "#72040a", "#5b0308", "#440206", "#2d0104",
        "#160002", "#000000"};

    static String biruMuda[] = {"#44e0ed", "#3dc9d5", "#36b3bd", "#2f9ca5", "#28868e", "#227076", "#1b595e", "#144347",
        "#0d2c2f", "#061617", "#000000"};

    static String hijau[] = {"#136955", "#115e4c", "#0f5444", "#0d493b", "#0b3f33", "#09342a", "#072a22", "#051f19", "#031511",
        "#010a08", "#000000"};

    static String messages[] =  {"loading", "downloading data", "install patch", "connecting",
        "checking data", "getting info", "almost done", "updating", "getting ready"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mainLayout = findViewById(R.id.main_layout);
        progressBar = findViewById(R.id.progres_bar);
        txtLoading = findViewById(R.id.txt_loading);

        initTimer();
        initTimerPesan();
    }

    private void initTimer(){
        timer = new CountDownTimer(3600000, 125) {
            @Override
            public void onTick(long l) {
                String warna = getWarna();

                initWindow(warna);
            }

            @Override
            public void onFinish() {
                timer.cancel();
                initTimer();
            }
        }.start();
    }

    private void initTimerPesan(){
        timer2 = new CountDownTimer(3600000, 223) {
            @Override
            public void onTick(long l) {
                long timestamp = Calendar.getInstance().getTimeInMillis();
                long modulo = timestamp % 8;
                if(modulo == 4 || modulo == 6){
                    int indexMessage = kodeWarna % 9;
                    txtLoading.setText(messages[indexMessage]);
                }
            }

            @Override
            public void onFinish() {
                timer2.cancel();
                initTimerPesan();
            }
        }.start();
    }

    private String getWarna(){
        if(kodeWarna > 80){
            kodeWarna = 0;
            counter = 10;
            mundur = true;
        }

        String warna =  "";
        if(kodeWarna <= 20){
            warna = orange[counter];
        }

        if(kodeWarna > 20 && kodeWarna <= 40){
            warna = merah[counter];
        }

        if(kodeWarna > 40 && kodeWarna <= 60){
            warna = biruMuda[counter];
        }

        if(kodeWarna > 60 && kodeWarna <= 80){
            warna = hijau[counter];
        }

        if(mundur){
            counter--;
        }else {
            counter++;
        }

        if(counter == 0){
            mundur = false;
        }

        if(counter == 10){
            mundur = true;
        }

        kodeWarna++;

        return warna;
    }

    private void initWindow(String warna){
        getWindow().setStatusBarColor(Color.parseColor(warna));
        mainLayout.setBackgroundColor(Color.parseColor(warna));
    }
}
